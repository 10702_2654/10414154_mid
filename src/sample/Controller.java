package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

import javax.swing.*;
import java.util.TimerTask;

public class Controller {
    public Button btn3_1;
    public Button btn5_1;
    public Button btn8_2;
    public Button btn3_2;
    public Button btn5_2;
    public Button btn4_1;
    public Button btn1_1;
    public Button btn2_1;
    public Button btn1_2;
    public Button btn2_2;
    public Button btn6_1;
    public Button btn8_1;
    public Button btn7_1;
    public Button btn4_2;
    public Button btn7_2;
    public Button btn6_2;
    public Label time;
    private int a = 0, b = 0;

    public void onMouseClick(ActionEvent actionEvent) {
        btn1_1.setOnMouseClicked(event -> {
            btn1_1.setText("1");
            a = 1;
        });
        btn1_2.setOnMouseClicked(event -> {
            btn1_2.setText("1");
            b = 1;
        });
        btn2_1.setOnMouseClicked(event -> {
            btn2_1.setText("2");
            a = 2;
        });
        btn2_2.setOnMouseClicked(event -> {
            btn2_2.setText("2");
            b = 2;
        });
        btn3_1.setOnMouseClicked(event -> {
            btn3_1.setText("3");
            a = 3;
        });
        btn3_2.setOnMouseClicked(event -> {
            btn3_2.setText("3");
            b = 3;
        });
        btn4_1.setOnMouseClicked(event -> {
            btn4_1.setText("4");
            a = 4;
        });
        btn4_2.setOnMouseClicked(event -> {
            btn4_2.setText("4");
            b = 4;
        });
        btn5_1.setOnMouseClicked(event -> {
            btn5_1.setText("5");
            a = 5;
        });
        btn5_2.setOnMouseClicked(event -> {
            btn5_2.setText("5");
            b = 5;
        });
        btn6_1.setOnMouseClicked(event -> {
            btn6_1.setText("6");
            a = 6;
        });
        btn6_2.setOnMouseClicked(event -> {
            btn6_2.setText("6");
            b = 6;
        });
        btn7_1.setOnMouseClicked(event -> {
            btn7_1.setText("7");
            a = 7;
        });
        btn7_2.setOnMouseClicked(event -> {
            btn7_2.setText("7");
            b = 7;
        });
        btn8_1.setOnMouseClicked(event -> {
            btn8_1.setText("8");
            a = 8;
        });
        btn8_2.setOnMouseClicked(event -> {
            btn8_2.setText("8");
            b = 8;
        });

        if (a==b){
            switch (a){
                case 1:
                    btn1_1.setDisable(true);
                    btn1_2.setDisable(true);
                    break;
                case 2:
                    btn2_1.setDisable(true);
                    btn2_2.setDisable(true);
                    break;
                case 3:
                    btn3_1.setDisable(true);
                    btn3_2.setDisable(true);
                    break;
                case 4:
                    btn4_1.setDisable(true);
                    btn4_2.setDisable(true);
                    break;
                case 5:
                    btn5_1.setDisable(true);
                    btn5_2.setDisable(true);
                    break;
                case 6:
                    btn6_1.setDisable(true);
                    btn6_2.setDisable(true);
                    break;
                case 7:
                    btn7_1.setDisable(true);
                    btn7_2.setDisable(true);
                    break;
                case 8:
                    btn8_1.setDisable(true);
                    btn8_2.setDisable(true);
                    break;

            }

        }else {
            btn1_1.setText("btn");
            btn1_2.setText("btn");
            btn2_1.setText("btn");
            btn2_2.setText("btn");
            btn3_1.setText("btn");
            btn3_2.setText("btn");
            btn4_1.setText("btn");
            btn4_2.setText("btn");
            btn5_1.setText("btn");
            btn5_2.setText("btn");
            btn6_1.setText("btn");
            btn6_2.setText("btn");
            btn7_1.setText("btn");
            btn7_2.setText("btn");
            btn8_1.setText("btn");
            btn8_2.setText("btn");
        }

        if (btn1_1.isDisabled()&&btn1_2.isDisabled()&&btn2_1.isDisabled()&&btn2_2.isDisabled()&&btn3_1.isDisabled()&&btn3_2.isDisabled()
                &&btn4_1.isDisabled()&&btn4_2.isDisabled()&&btn5_1.isDisabled()&&btn5_2.isDisabled()&&btn6_1.isDisabled()&&btn6_2.isDisabled()
                &&btn7_1.isDisabled()&&btn7_2.isDisabled()&&btn8_1.isDisabled()&&btn8_2.isDisabled()){
            JOptionPane.showMessageDialog(null, "恭喜過關~");
        }

       
    }

    }



